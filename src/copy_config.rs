use std::borrow::Cow;
use std::fs;

use anyhow::{Context, Result};
use serde_yaml;
use shellexpand::tilde;

use crate::copy_condition::{CopyCondition, SerdeCopyConfig};

pub fn read_config() -> Result<Vec<CopyCondition>> {
    read_custom_user_config()
}

fn read_custom_user_config() -> Result<Vec<CopyCondition>> {
    let user_custom_config = tilde("~/.config/clementine_utils/copier/copy_config.yml");
    let yaml = load_yaml_from_file(user_custom_config)?;
    Ok(CopyCondition::from_yaml(yaml))
}

fn load_yaml_from_file(file_name: Cow<str>) -> Result<Vec<SerdeCopyConfig>> {
    let file = fs::File::open(file_name.as_ref()).context(format!("cannot open {}", file_name))?;
    let data: Vec<SerdeCopyConfig> =
        serde_yaml::from_reader(file).context(format!("cannot parse {}", file_name))?;
    Ok(data)
}
