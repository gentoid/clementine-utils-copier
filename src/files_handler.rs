use std::{
    fs::{copy, remove_file},
    path::PathBuf,
};

use crate::{database::SrcFile, destination::DestFile, diff::Diff};
use colored::Colorize;

pub fn handle_diff(diff: &Diff) {
    delete_files(&diff.to_delete);
    copy_files(&diff.to_add, &diff.destination.condition.path);
    copy_files(&diff.to_update, &diff.destination.condition.path);
}

fn copy_files(files: &Vec<SrcFile>, destination: &PathBuf) {
    for db_file in files {
        let mut dst = destination.clone();
        println!(
            "Copying {} ...",
            db_file.filename.to_string_lossy().magenta()
        );
        dst.push(&db_file.filename);
        match copy(db_file.path.clone(), dst) {
            Ok(_) => (),
            Err(err) => println!("Error {}", err),
        }
    }
}

fn delete_files(files: &Vec<DestFile>) {
    for file in files {
        println!("Deleting {} ...", file.path.to_string_lossy().red());
        match remove_file(file.path.clone()) {
            Ok(_) => (),
            Err(err) => println!("Error {}", err),
        }
    }
}
