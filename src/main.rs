extern crate clementine_utils_copier as copier;

use std::path::PathBuf;

use anyhow::Result;
use rusqlite::{Connection, OpenFlags};
use shellexpand::tilde;

use copier::copy_config;
use copier::database as db;
use copier::destination;
use copier::diff;
use copier::files_handler;

fn main() -> Result<()> {
    let db_file = PathBuf::from(tilde("~/.config/Clementine/clementine.db").as_ref());
    let conn = Connection::open_with_flags(db_file, OpenFlags::SQLITE_OPEN_READ_ONLY).unwrap();

    let config = copy_config::read_config()?;

    for cond in config {
        match db::get_filenames(&conn, &cond) {
            Err(err) => println!("{}", err),
            Ok(res) => {
                let dest_files = destination::get_files(cond.clone())?;
                let diff = diff::diff(res, dest_files)?;
                println!("{}", diff.formatted());
                files_handler::handle_diff(&diff);
            }
        }
    }

    Ok(())
}
