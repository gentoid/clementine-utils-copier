use crate::{
    database::SrcFile,
    destination::{DestFile, Destination},
};
use anyhow::{Context, Result};
use colored::{Color, Colorize};
use std::{fs::metadata, path::PathBuf, time::SystemTime};

#[derive(Debug)]
pub struct Diff {
    pub src_files: Vec<SrcFile>,
    pub destination: Destination,
    pub keep: Vec<DestFile>,
    pub to_delete: Vec<DestFile>,
    pub to_add: Vec<SrcFile>,
    pub to_update: Vec<SrcFile>,
}

pub fn diff(src_files: Vec<SrcFile>, destination: Destination) -> Result<Diff> {
    let mut keep = vec![];
    let mut to_delete = vec![];
    let mut to_add = vec![];
    let mut to_update = vec![];

    for dest_file in &destination.files {
        let dest_file = dest_file.clone();

        match src_files
            .iter()
            .find(|src_file| src_file.filename == dest_file.filename)
        {
            Some(src_file) => {
                let dest_modified = get_modified(&dest_file.path)?;
                let src_modified = get_modified(&src_file.path)?;

                if src_modified > dest_modified {
                    to_update.push(src_file.clone());
                } else {
                    keep.push(dest_file.clone());
                }
            }
            None => {
                to_delete.push(dest_file.clone());
            }
        }
    }

    for src_file in &src_files {
        let db_filename = src_file.filename.clone();

        if let None = destination
            .files
            .iter()
            .find(|file| file.filename == db_filename)
        {
            to_add.push(src_file.clone());
        }
    }

    Ok(Diff {
        src_files,
        destination,
        to_delete,
        to_add,
        keep,
        to_update,
    })
}

fn get_modified(path: &PathBuf) -> Result<SystemTime> {
    let metadata = metadata(&path).context(format!("unable to get metadata of {:?}", path))?;
    let modified = metadata
        .modified()
        .context(format!("unable to get modified time of {:?}", path))?;

    Ok(modified)
}

impl Diff {
    pub fn formatted(&self) -> String {
        let path = self.destination.condition.path.clone();

        let additions = self
            .to_add
            .iter()
            .map(|db_file| {
                format!(
                    "  {}",
                    format_path(&db_file.path.clone(), Color::Green, Color::BrightGreen)
                )
            })
            .collect::<Vec<String>>()
            .join("\n");

        let deletions = self
            .to_delete
            .iter()
            .map(|det_file| {
                format!(
                    "  {}",
                    format_path(&det_file.path, Color::Red, Color::BrightRed)
                )
            })
            .collect::<Vec<String>>()
            .join("\n");

        let updates = self
            .to_update
            .iter()
            .map(|file| {
                format!(
                    "  {}",
                    format_path(&file.path, Color::Blue, Color::BrightBlue)
                )
            })
            .collect::<Vec<String>>()
            .join("\n");

        format!(
            "{path}\n{additions}\n{updates}\n{deletions}",
            path = path.into_os_string().into_string().unwrap().bright_blue(),
            additions = additions,
            updates = updates,
            deletions = deletions,
        )
    }
}

fn format_path(path: &PathBuf, color1: Color, color2: Color) -> String {
    let path1 = path.parent().unwrap().to_str().unwrap().to_string();
    let path2 = path.file_name().unwrap().to_str().unwrap().to_string();
    format!(
        "{}{}",
        format!("{}/", path1).color(color1),
        path2.color(color2)
    )
}
