use std::path::PathBuf;

use anyhow::{anyhow, Result};
use serde_derive::Deserialize;
use shellexpand::tilde;

#[derive(Debug, Clone)]
pub struct CopyCondition {
    pub path: PathBuf,
    pub ratings: Vec<u8>,
}

impl CopyCondition {
    pub fn from_yaml(data: Vec<SerdeCopyConfig>) -> Vec<Self> {
        data.iter()
            .flat_map(|item| item.to_copy_conditions(None))
            .collect()
    }

    pub fn str_path(&self) -> Result<&str> {
        self.path.to_str().ok_or(anyhow!(format!(
            "unable to convert '{:?}' to str",
            self.path
        )))
    }
}

#[derive(Deserialize, Debug)]
struct SerdeConditions {
    ratings: Vec<u8>,
}

#[derive(Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct SerdeCopyConfig {
    path: PathBuf,
    #[serde(default = "default_destination_value")]
    destination: bool,
    conditions: Option<SerdeConditions>,
    subpaths: Option<Vec<SerdeCopyConfig>>,
}

fn default_destination_value() -> bool {
    false
}

impl SerdeCopyConfig {
    fn to_copy_conditions(&self, parent_path: Option<&PathBuf>) -> Vec<CopyCondition> {
        let full_path = match parent_path {
            None => self.path.clone(),
            Some(path) => {
                let mut new_path = path.clone();
                new_path.push(&self.path);
                new_path
            }
        };
        let my_own_cc: Option<CopyCondition> = if self.destination {
            self.conditions.as_ref().map(|conds| CopyCondition {
                path: PathBuf::from(tilde(&full_path.to_str().unwrap()).as_ref()),
                ratings: conds.ratings.clone(),
            })
        } else {
            None
        };
        let maybe_sub_ccs = self.subpaths.as_ref().map(|subpaths| {
            subpaths
                .iter()
                .flat_map(|sub_data| sub_data.to_copy_conditions(Some(&full_path)))
                .collect::<Vec<_>>()
        });

        match (my_own_cc, maybe_sub_ccs) {
            (None, None) => vec![],
            (Some(cc), None) => vec![cc],
            (None, Some(sub_ccs)) => sub_ccs,
            (Some(cc), Some(mut sub_ccs)) => {
                sub_ccs.push(cc);
                sub_ccs
            }
        }
    }
}
