use std::{ffi::OsString, path::PathBuf};

use anyhow::{anyhow, Context, Result};
use rusqlite::{Connection, NO_PARAMS};
use url::Url;

use crate::copy_condition::CopyCondition;

#[derive(Debug, Clone)]
pub struct SrcFile {
    pub path: PathBuf,
    pub filename: OsString,
}

pub fn get_filenames(conn: &Connection, condition: &CopyCondition) -> Result<Vec<SrcFile>> {
    let ratings_part = condition
        .ratings
        .iter()
        .map(|&rating| format!("ABS(rating - {}) < 0.01", f32::from(rating) / 10.0))
        .collect::<Vec<_>>()
        .join(" OR ");
    let query = format!(
        "SELECT filename FROM songs WHERE unavailable IS FALSE AND ({}) AND lastplayed > 0",
        ratings_part
    );
    let mut stmt = conn.prepare(query.as_str())?;
    let rows = stmt
        .query_map(NO_PARAMS, |row| row.get(0))
        .context(format!("cannot query DB with '{}'", query))?;

    let mut response = vec![];
    for row in rows {
        match parse_path(row?) {
            Err(error) => println!("{}", error),
            Ok(row) => response.push(row),
        }
    }

    Ok(response)
}

fn parse_path(db_filename: Vec<u8>) -> Result<SrcFile> {
    let filename = std::str::from_utf8(&db_filename).context("cannot convert Vec<u8> to str")?;
    let parsed = Url::parse(filename).context(format!("cannot parse URL from '{}'", filename))?;
    let path = parsed
        .to_file_path()
        .or(Err(anyhow!("cannot convert '{}' to file path", parsed)))?;

    Ok(SrcFile {
        path: path.clone(),
        filename: get_filename(&path)?,
    })
}

pub fn get_filename(path: &PathBuf) -> Result<OsString> {
    Ok(path
        .file_name()
        .context(format!("could not get filename of '{:?}'", path))?
        .to_os_string())
}
