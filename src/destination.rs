use crate::{copy_condition::CopyCondition, database::get_filename};
use anyhow::{Context, Result};
use glob::glob;
use std::{ffi::OsString, path::PathBuf};

#[derive(Debug)]
pub struct Destination {
    pub files: Vec<DestFile>,
    pub condition: CopyCondition,
}

#[derive(Debug, Clone)]
pub struct DestFile {
    pub path: PathBuf,
    pub filename: OsString,
}

pub fn get_files(condition: CopyCondition) -> Result<Destination> {
    let pattern = format!("{}/*", condition.str_path()?);
    let paths =
        glob(pattern.as_str()).context(format!("unable to find files by '{}' pattern", pattern))?;

    let mut list = vec![];

    for path in paths {
        let path = path.context("cannot get path from paths")?;
        list.push(DestFile {
            path: path.clone(),
            filename: get_filename(&path)?,
        })
    }

    Ok(Destination {
        files: list,
        condition,
    })
}
